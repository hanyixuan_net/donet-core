using System.Collections.Generic;
using JwtAuthDemo.Entities;
namespace JwtAuthDemo.IServices
{
    public interface IUserService
    {
        User Authenticate(string name,string password);
        IEnumerable<User> GetAll();
    }
}