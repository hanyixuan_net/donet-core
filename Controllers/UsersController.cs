using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using JwtAuthDemo.IServices;
using System.Linq;
using JwtAuthDemo.Models;
namespace JwtAuthDemo.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserController:ControllerBase
    {
        private IUserService _userService;
        public UserController(IUserService userService)
        {
          _userService=userService;
        }
         [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            var user=_userService.Authenticate(model.UserName,model.Password);
            if(User==null)
            {
                return BadRequest(new {message="用户名或者密码不正确"});
            }
            return Ok(user);
        }
        public IActionResult GetAll()
        {
            var users=_userService.GetAll();
            return Ok(users);
        }

    }
}